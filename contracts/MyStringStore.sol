pragma solidity ^0.4.24;

contract MyStringStore {
    
    /// @notice The stored string
    string public myString = "Hello world";

    /// @notice Sets myString to a given string
    /// @param _str The string to pass
    function set(string _str) public {
        myString = _str;
    }
}